import React from 'react';
import ReactDOM from 'react-dom';

import { Root } from './components/root/root';

import { BrowserRouter } from 'react-router-dom';
import { SnackbarProvider } from 'notistack';
import * as serviceWorker from './serviceWorker';

import './index.css';

ReactDOM.render(
  <BrowserRouter basename={process.env.PUBLIC_URL}>
    <SnackbarProvider
      maxSnack={1}
      anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
      preventDuplicate
    >
      <Root />
    </SnackbarProvider>
  </BrowserRouter>,

  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
