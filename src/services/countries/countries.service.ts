import { http } from '../http/http.service';

type Country = import('../../models/country').Country;

export async function getAllContries(): Promise<Country[]> {
  const response = await http.get<Country[]>('/all');
  return response.data;
}
