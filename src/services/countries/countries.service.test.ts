import { getAllContries } from './countries.service';

import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

const apiUrl = process.env.REACT_APP_API_URL;

describe('countries service', () => {
  it('should use correct URL', async () => {
    const mock = new MockAdapter(axios);
    mock.onGet(`${apiUrl}/all`).reply(200);

    await getAllContries();
    expect(mock.history.get.length).toBe(1);
    expect(mock.history.get[0].url).toBe(`${apiUrl}/all`);
  });
});
