import { http } from './http.service';

import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

const apiUrl = process.env.REACT_APP_API_URL;

describe('http service', () => {
  it('should use correct base URL', async () => {
    const mock = new MockAdapter(axios);
    mock.onGet(`${apiUrl}/test`).reply(200);

    await http.get('test');
    expect(mock.history.get.length).toBe(1);
    expect(mock.history.get[0].url).toBe(`${apiUrl}/test`);
  });
});
