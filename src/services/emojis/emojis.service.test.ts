import { generateFlag } from './emojis.service';

describe('emoji service', () => {
  it('should return 🐶 for not valid codes', async () => {
    expect(generateFlag('')).toBe('🐶');
    expect(generateFlag('a')).toBe('🐶');
    expect(generateFlag('aaa')).toBe('🐶');
    expect(generateFlag('@Z')).toBe('🐶');
    expect(generateFlag('[Z')).toBe('🐶');
    expect(generateFlag('A[')).toBe('🐶');
    expect(generateFlag('A@')).toBe('🐶');
  });

  it('should return valid flag for not valid codes', async () => {
    expect(generateFlag('AF')).toBe('🇦🇫');
  });

  it('should return from cache', async () => {
    expect(generateFlag('AF')).toBe('🇦🇫');
    expect(generateFlag('AF')).toBe('🇦🇫');
  });
});
