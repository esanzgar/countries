const A = 'A'.charCodeAt(0);
const Z = 'Z'.charCodeAt(0);
const flagACode = 127462 - A;

export const generateFlag = (function() {
  // Performance improvement by memoizing the results
  const memory: Record<string, string> = {};

  return (code: string) => {
    const cache = memory[code];
    if (cache) {
      return cache;
    }

    if (code.length !== 2) {
      return '🐶';
    }

    const firstCode = code.charCodeAt(0);
    const secondCode = code.charCodeAt(1);
    if (firstCode < A || firstCode > Z || secondCode < A || secondCode > Z) {
      return '🐶';
    }

    const result = String.fromCodePoint(
      flagACode + firstCode,
      flagACode + secondCode
    );
    memory[code] = result;

    return result;
  };
})();
