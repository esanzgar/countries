import React from 'react';

import { Card } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';

export const NotFound = () => {
  return (
    <Card>
      <Card.Body>
        <Card.Title>
          <FontAwesomeIcon icon={faExclamationTriangle} size="lg" />
          Country not found
        </Card.Title>
        <LinkContainer to="/all">
          <Card.Link>Go to countries of the world</Card.Link>
        </LinkContainer>
      </Card.Body>
    </Card>
  );
};
