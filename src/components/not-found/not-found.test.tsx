import React from 'react';
import { shallow } from 'enzyme';

import { NotFound } from './not-found';

describe('<NotFound />', () => {
  it('should display', () => {
    const wrapper = shallow(<NotFound />);
    expect(wrapper).toMatchSnapshot();
  });
});
