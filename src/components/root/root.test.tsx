import React from 'react';
import { shallow } from 'enzyme';

import { Root } from './root';

describe('<Root />', () => {
  it('should display', () => {
    const wrapper = shallow(<Root />);
    expect(wrapper).toMatchSnapshot();
  });
});
