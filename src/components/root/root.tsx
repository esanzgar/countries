import React, { useEffect, useState, lazy, Suspense } from 'react';

import { Switch, Route, Redirect } from 'react-router-dom';
import { Container, Row, Col } from 'react-bootstrap';
import { Notifier } from '../notifier/notifier';
import { NavigationBar } from '../navigation-bar/navigation-bar';
import { ProgressBar } from '../progress-bar/progress-bar';
import { NotFound } from '../not-found/not-found';

import { getAllContries } from '../../services/countries/countries.service';
import { countryList, countryDict } from '../../data/countries';

type Country = import('../../models/country').Country;
type State = { list: Country[]; dictionary: Record<string, Country> };

const AllCountries = lazy(() =>
  import('../all-countries/all-countries').then(({ AllCountries }) => ({
    default: AllCountries
  }))
);

const SingleCountry = lazy(() =>
  import('../single-country/single-country').then(({ SingleCountry }) => ({
    default: SingleCountry
  }))
);

export const Root = () => {
  const [countries, setCountries] = useState<State>({
    list: countryList,
    dictionary: countryDict
  });

  useEffect(() => {
    const fetchData = async () => {
      const list = await getAllContries();
      const dictionary: Record<string, Country> = {};
      list.forEach(country => (dictionary[country.alpha3Code] = country));
      setCountries({ list, dictionary });
    };
    fetchData();
  }, []);

  return (
    <>
      <Notifier />
      <NavigationBar />
      <ProgressBar />

      <Container>
        <Row>
          <Col sm={0} md={2}></Col>
        </Row>
        <Row>
          <Col>
            <Suspense fallback={null}>
              <Switch>
                <Route
                  path="/all"
                  render={() => <AllCountries countries={countries.list} />}
                />
                <Route
                  path="/country/:code([A-Z]{3})"
                  render={() => (
                    <SingleCountry countries={countries.dictionary} />
                  )}
                />
                <Route
                  path="/country/:code([a-z]{3})"
                  render={({ match }) => (
                    <Redirect
                      to={`/country/${match.params.code.toUpperCase()}`}
                    />
                  )}
                />
                <Route path="/not-found" component={NotFound} />
                <Redirect exact from="/" to="/all" />
                <Redirect to="/not-found" />
              </Switch>
            </Suspense>
          </Col>
        </Row>
        <Row>
          <Col sm={0} md={2}></Col>
        </Row>
      </Container>
    </>
  );
};
