import React from 'react';
import { shallow } from 'enzyme';

import { ProgressBar } from './progress-bar';

describe('<ProgressBar />', () => {
  it('should display', () => {
    const wrapper = shallow(<ProgressBar />);
    expect(wrapper).toMatchSnapshot();
  });
});
