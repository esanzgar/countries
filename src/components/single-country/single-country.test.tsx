import React from 'react';
import { shallow } from 'enzyme';

import { SingleCountry } from './single-country';

import { countryDict } from '../../data/countries';

describe('<SingleCountry />', () => {
  it('should display', () => {
    const wrapper = shallow(<SingleCountry countries={countryDict} />);
    expect(wrapper).toMatchSnapshot();
  });
});
