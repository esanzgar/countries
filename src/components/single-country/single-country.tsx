import React from 'react';

import { withRouter, Link, Redirect } from 'react-router-dom';
import { Card } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { CountryFacts } from '../country-facts/country-facts';
import { WorldMap } from '../world-map/world-map';

import { generateFlag } from '../../services/emojis/emojis.service';

type Country = import('../../models/country').Country;
type Props = {
  countries: Record<string, Country>;
} & import('react-router-dom').RouteComponentProps;

export const SingleCountry = withRouter(({ match, countries }: Props) => {
  const code = (match.params as Record<string, string>)['code'];
  const myCountry = countries[code];

  if (!myCountry) {
    return <Redirect to="/not-found" />;
  }

  const { name, nativeName, borders, alpha2Code } = myCountry;

  return (
    <Card>
      <WorldMap code2={alpha2Code} />

      <Card.Body>
        <Card.Title as="h2">
          {name !== nativeName ? `${name} \u2014 ${nativeName}` : name}
          <span className="ml-2 flag">{generateFlag(alpha2Code)}</span>
        </Card.Title>

        <div className="mt-4">
          <CountryFacts country={myCountry} />
        </div>

        {borders.length > 0 ? (
          <>
            <h5 className="mt-4">Neighbouring countries</h5>
            <ul>
              {borders.map(neighbour => (
                <li key={neighbour}>
                  <Link
                    to={`/country/${neighbour}`}
                    onClick={() => document.body.scrollIntoView(true)}
                  >
                    {countries[neighbour].name}
                  </Link>
                </li>
              ))}
            </ul>
          </>
        ) : null}

        <LinkContainer to="/all">
          <Card.Link>Go to countries of the world</Card.Link>
        </LinkContainer>
      </Card.Body>
    </Card>
  );
});
