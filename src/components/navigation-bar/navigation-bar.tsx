import React from 'react';

import { Navbar } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

import styles from './navigation-bar.module.css';

import logo from './logo.png';

export const NavigationBar = () => {
  return (
    <Navbar
      bg="dark"
      variant="dark"
      sticky="top"
      className={styles.primaryColor}
    >
      <LinkContainer to="/all">
        <Navbar.Brand>
          <img
            alt="World countries logo"
            src={logo}
            width="30"
            height="30"
            className="d-inline-block align-top"
          />{' '}
          Countries of the world
        </Navbar.Brand>
      </LinkContainer>
    </Navbar>
  );
};
