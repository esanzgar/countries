import React from 'react';
import { shallow } from 'enzyme';

import { NavigationBar } from './navigation-bar';

describe('<NavigationBar />', () => {
  it('should display', () => {
    const wrapper = shallow(<NavigationBar />);
    expect(wrapper).toMatchSnapshot();
  });
});
