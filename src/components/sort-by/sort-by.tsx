import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faSortAmountUp,
  faSortAmountDown,
  faSortAlphaUp,
  faSortAlphaDown
} from '@fortawesome/free-solid-svg-icons';

type Props = {
  onClick: VoidFunction;
  variant: Category;
  filtering: {
    category: Category;
    desc: boolean;
  };
};

type Category = 'name' | 'population';

export const SortBy = ({ onClick, variant, filtering }: Props) => {
  let title = 'Country name';
  let faDown = faSortAlphaDown;
  let faUp = faSortAlphaUp;

  if (variant === 'population') {
    title = 'Population';
    faDown = faSortAmountDown;
    faUp = faSortAmountUp;
  }

  return (
    <span tabIndex={0.1} onClick={onClick} className="cursor">
      {title}{' '}
      {variant !== filtering.category ? (
        <>
          <FontAwesomeIcon icon={faDown} />
          <span className="sr-only">{`Sort countries by ${variant} in descending order`}</span>
        </>
      ) : (
        <>
          <FontAwesomeIcon icon={filtering.desc ? faUp : faDown} />
          <span className="sr-only">{`Sort countries by ${variant} in ${
            filtering.desc ? 'ascending' : 'descending'
          } order`}</span>
        </>
      )}
    </span>
  );
};
