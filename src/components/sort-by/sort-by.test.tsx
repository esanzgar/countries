import React from 'react';
import { shallow } from 'enzyme';

import { SortBy } from './sort-by';

describe('<NotFound />', () => {
  it('should display country name', () => {
    const wrapper = shallow(
      <SortBy
        variant="name"
        filtering={{ category: 'population', desc: true }}
        onClick={() => null}
      />
    );
    expect(wrapper).toHaveText(
      'Country name <FontAwesomeIcon />Sort countries by name in descending order'
    );
  });

  it('should display population', () => {
    const wrapper = shallow(
      <SortBy
        variant="population"
        filtering={{ category: 'name', desc: true }}
        onClick={() => null}
      />
    );
    expect(wrapper).toHaveText(
      'Population <FontAwesomeIcon />Sort countries by population in descending order'
    );
  });

  it('variant "name" should be displayed in all 4 possible scenerios', () => {
    expect(
      shallow(
        <SortBy
          variant="name"
          filtering={{ category: 'name', desc: true }}
          onClick={() => null}
        />
      )
    ).toMatchSnapshot();

    expect(
      shallow(
        <SortBy
          variant="name"
          filtering={{ category: 'name', desc: false }}
          onClick={() => null}
        />
      )
    ).toMatchSnapshot();

    expect(
      shallow(
        <SortBy
          variant="name"
          filtering={{ category: 'population', desc: true }}
          onClick={() => null}
        />
      )
    ).toMatchSnapshot();

    expect(
      shallow(
        <SortBy
          variant="name"
          filtering={{ category: 'population', desc: false }}
          onClick={() => null}
        />
      )
    ).toMatchSnapshot();
  });

  it('variant "name" should be displayed in all 4 possible scenerios', () => {
    expect(
      shallow(
        <SortBy
          variant="population"
          filtering={{ category: 'population', desc: true }}
          onClick={() => null}
        />
      )
    ).toMatchSnapshot();

    expect(
      shallow(
        <SortBy
          variant="population"
          filtering={{ category: 'population', desc: false }}
          onClick={() => null}
        />
      )
    ).toMatchSnapshot();

    expect(
      shallow(
        <SortBy
          variant="population"
          filtering={{ category: 'name', desc: true }}
          onClick={() => null}
        />
      )
    ).toMatchSnapshot();

    expect(
      shallow(
        <SortBy
          variant="population"
          filtering={{ category: 'name', desc: false }}
          onClick={() => null}
        />
      )
    ).toMatchSnapshot();
  });
});
