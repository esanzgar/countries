import React, { useState, useEffect } from 'react';

import { withRouter } from 'react-router-dom';
import { Table, Card } from 'react-bootstrap';
import { FilterCountry } from '../filter-country/filter-country';
import { SortBy } from '../sort-by/sort-by';

import { generateFlag } from '../../services/emojis/emojis.service';

import styles from './all-countries.module.css';

type Country = import('../../models/country').Country;
type Category = Parameters<typeof SortBy>[0]['variant'];
type Props = {
  countries: Country[];
} & import('react-router-dom').RouteComponentProps;

export const AllCountries = withRouter(({ history, countries = [] }: Props) => {
  const [filtering, setFiltering] = useState({
    sortedFiltered: countries,
    category: 'name' as Category,
    desc: true,
    keyword: ''
  });

  useEffect(() => {
    setFiltering(filtering => {
      const { category, desc, keyword } = filtering;
      let sortedFiltered = [...countries];

      // Reset the filtering and sorting (only when different than default values)
      if (keyword) {
        sortedFiltered = sortedFiltered.filter(generateFiltering(keyword));
      }

      if (category !== 'name' || desc !== true) {
        sortedFiltered.sort(generateSorting(category, desc));
      }

      return { ...filtering, sortedFiltered };
    });
  }, [countries]);

  const filterCountries = (keyword: string) => {
    // Reset
    if (!keyword) {
      setFiltering({ ...filtering, sortedFiltered: countries, keyword: '' });
      return;
    }

    try {
      // Not perfect search because:
      // 1. It is not fuzzy: searching for keyword 'a' doesn't return 'Å.'
      // 2. Users don't think on term of regular expressions: keyword '(' will
      //    cause an error. Keyword '\(' searches for open parenthesis.
      const sortedFiltered = countries.filter(generateFiltering(keyword));
      const { category, desc } = filtering;
      sortedFiltered.sort(generateSorting(category, desc));
      setFiltering({ ...filtering, sortedFiltered, keyword });
    } catch (error) {
      // Errors often arise compiling the regular expression above
      setFiltering({ ...filtering, sortedFiltered: [], keyword });
    }
  };

  function toogleSort(category: Category) {
    return () => {
      let desc = !filtering.desc;
      if (category !== filtering.category) {
        desc = true;
      }

      filtering.sortedFiltered.sort(generateSorting(category, desc));
      const sortedFiltered = [...filtering.sortedFiltered];

      setFiltering({
        ...filtering,
        sortedFiltered,
        category,
        desc
      });
    };
  }

  function generateFiltering(keyword: string) {
    const pattern = new RegExp(keyword, 'i');
    return (country: Country) => pattern.test(country.name);
  }

  function generateSorting(category: Category, desc: boolean) {
    if (category === 'name') {
      return (country1: Country, country2: Country) => {
        const value = country1[category].localeCompare(
          country2[category],
          'en',
          { sensitivity: 'base' }
        );
        return desc ? value : -value;
      };
    }
    return (country1: Country, country2: Country) => {
      const value = country2[category] - country1[category];
      return desc ? value : -value;
    };
  }

  const { sortedFiltered, ...filteringState } = filtering;

  return (
    <Card>
      <Card.Body>
        <FilterCountry onFilter={filterCountries} />

        <Table responsive striped hover size="sm">
          <thead>
            <tr>
              <th>#</th>
              <th>
                <SortBy
                  onClick={toogleSort('name')}
                  variant="name"
                  filtering={filteringState}
                />
              </th>
              <th>
                <SortBy
                  onClick={toogleSort('population')}
                  variant="population"
                  filtering={filteringState}
                />
              </th>
              <th>Flag</th>
            </tr>
          </thead>

          <tbody className={styles.center}>
            {sortedFiltered.map((country, index) => (
              <tr
                key={country.alpha3Code}
                tabIndex={0.1}
                className="cursor"
                onClick={() => history.push(`/country/${country.alpha3Code}`)}
                area-label={country.name}
              >
                <td>{index + 1}</td>
                <td>{country.name}</td>
                <td>
                  <span>
                    {country.population.toLocaleString(undefined, {
                      useGrouping: true
                    })}
                  </span>
                </td>
                <td>
                  <span className="flag">
                    {generateFlag(country.alpha2Code)}
                  </span>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Card.Body>
    </Card>
  );
});
