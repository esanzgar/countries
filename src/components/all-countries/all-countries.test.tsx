import React from 'react';
import { shallow } from 'enzyme';

import { AllCountries } from './all-countries';

import { countryList } from '../../data/countries';

describe('<AllCountries />', () => {
  it('should display', () => {
    const wrapper = shallow(<AllCountries countries={countryList} />);
    expect(wrapper).toMatchSnapshot();
  });
});
