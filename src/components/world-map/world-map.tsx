import React, { useEffect, useState, useRef } from 'react';

import world from './world.svg';
import styles from './world-map.module.css';

type Props = {
  code2: string;
};

export const WorldMap = ({ code2: code }: Props) => {
  const [svg, setSvg] = useState<Document | null>(null);
  const objectRef = useRef<HTMLObjectElement | null>(null);

  useEffect(() => {
    let country: HTMLElement | null = null;

    if (svg) {
      country = svg.getElementById(code);
      if (country) {
        const primaryColor = getComputedStyle(document.body).getPropertyValue(
          '--primary-color'
        );
        country.style.fill = primaryColor;
      }
    }

    return () => {
      if (country) {
        const blackgroundColor = getComputedStyle(
          document.body
        ).getPropertyValue('--background-color');
        country.style.fill = blackgroundColor;
      }
    };
  }, [code, svg]);

  return (
    <object
      data={world}
      type="image/svg+xml"
      className={styles.worldMap}
      ref={objectRef}
      onLoad={a => setSvg(a.currentTarget.getSVGDocument())}
    >
      Map of the world
    </object>
  );
};
