import React from 'react';
import { shallow } from 'enzyme';

import { WorldMap } from './world-map';

describe('<WordMap />', () => {
  it('should display', () => {
    const wrapper = shallow(<WorldMap code2="CA" />);
    expect(wrapper).toMatchSnapshot();
  });
});
