import React from 'react';

import { Table } from 'react-bootstrap';

import styles from './country-facts.module.css';

type Country = import('../../models/country').Country;

export const CountryFacts = ({ country }: { country: Country }) => {
  const { population, area, capital, timezones } = country;

  const languages = country.languages.map(
    ({ name, nativeName }) =>
      `${name}${nativeName !== name && ` (${nativeName})`}`
  );

  const currencies = country.currencies.map(
    ({ name, symbol }) => `${name}${symbol && ` (${symbol})`}`
  );

  return (
    <Table responsive striped size="sm">
      <tbody>
        {capital ? (
          <tr>
            <td className={styles.topic}>Capital</td>
            <td>{capital}</td>
          </tr>
        ) : null}
        <tr>
          <td className={styles.topic}>Population</td>
          <td>{population.toLocaleString(undefined, { useGrouping: true })}</td>
        </tr>
        {area && (
          <tr>
            <td className={styles.topic}>Area</td>
            <td>
              {area.toLocaleString(undefined, { useGrouping: true })} km
              <sup>2</sup>
            </td>
          </tr>
        )}
        <tr>
          <td className={styles.topic}>
            {languages.length > 1 ? 'Languages' : 'Language'}
          </td>
          <td>{languages.join(', ')}</td>
        </tr>
        <tr>
          <td className={styles.topic}>
            {timezones.length > 1 ? 'Timezones' : 'Timezone'}
          </td>
          <td> {timezones.join(', ')} </td>
        </tr>
        <tr>
          <td className={styles.topic}>
            {currencies.length > 1 ? 'Currencies' : 'Currency'}
          </td>
          <td> {currencies.join(', ')} </td>
        </tr>
      </tbody>
    </Table>
  );
};
