import React from 'react';
import { shallow } from 'enzyme';

import { CountryFacts } from './country-facts';

import { countryDict } from '../../data/countries';

describe('<CountryFacts />', () => {
  it('should contain basic information', () => {
    const wrapper = shallow(<CountryFacts country={countryDict.AND} />);
    expect(wrapper.find('td').at(0)).toHaveText('Capital');
    expect(wrapper.find('td').at(2)).toHaveText('Population');
    expect(wrapper.find('td').at(4)).toHaveText('Area');
    expect(wrapper.find('td').at(6)).toHaveText('Language');
    expect(wrapper.find('td').at(8)).toHaveText('Timezone');
    expect(wrapper.find('td').at(10)).toHaveText('Currency');
    expect(wrapper).toMatchSnapshot();
  });

  it('should not contain capital', () => {
    const wrapper = shallow(<CountryFacts country={countryDict.ATA} />);
    expect(wrapper.find('td').at(0)).not.toHaveText('Capital');
    expect(wrapper).toMatchSnapshot();
  });

  it('should not contain area', () => {
    const wrapper = shallow(<CountryFacts country={countryDict.PSE} />);
    expect(wrapper.find('td').at(4)).not.toHaveText('Capital');
    expect(wrapper).toMatchSnapshot();
  });

  it('should have several languages', () => {
    const wrapper = shallow(<CountryFacts country={countryDict.AFG} />);
    expect(wrapper.find('td').at(6)).toHaveText('Languages');
    expect(wrapper).toMatchSnapshot();
  });

  it('should have several timezones', () => {
    const wrapper = shallow(<CountryFacts country={countryDict.AUS} />);
    expect(wrapper.find('td').at(8)).toHaveText('Timezones');
    expect(wrapper).toMatchSnapshot();
  });

  it('should have several currencies', () => {
    const wrapper = shallow(<CountryFacts country={countryDict.BLR} />);
    expect(wrapper.find('td').at(10)).toHaveText('Currencies');
    expect(wrapper).toMatchSnapshot();
  });
});
