import { useEffect } from 'react';

import { useSnackbar } from 'notistack';
import axios from 'axios';

export const Notifier = () => {
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    // Catch errors via axios interceptor
    const onSuccess = (response: import('axios').AxiosResponse) => response;
    const onError = (error: any) => {
      if (error.response && error.response.data) {
        const message = error.response.data.message;
        enqueueSnackbar(message, { variant: 'error' });
        return Promise.reject(error);
      }

      enqueueSnackbar('Unknown error, please try again.', { variant: 'error' });
      return Promise.reject(error);
    };
    const myInterceptor = axios.interceptors.response.use(onSuccess, onError);

    // On/offline detection
    const updateOnlineStatus = (event: Event) => {
      if (window.navigator.onLine) {
        enqueueSnackbar('You are back online.', { variant: 'success' });
      }
      enqueueSnackbar('You are offline.', { variant: 'error' });
    };
    window.addEventListener('online', updateOnlineStatus);
    window.addEventListener('offline', updateOnlineStatus);

    return () => {
      axios.interceptors.response.eject(myInterceptor);
      window.removeEventListener('online', updateOnlineStatus);
      window.removeEventListener('offline', updateOnlineStatus);
    };
  }, [enqueueSnackbar]);
  return null;
};
