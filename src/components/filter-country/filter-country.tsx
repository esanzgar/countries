import React from 'react';

import { InputGroup, FormControl } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
type Props = {
  onFilter: (keyword: string) => void;
};

export const FilterCountry = ({ onFilter }: Props) => {
  return (
    <InputGroup className="col-lg-5 float-right mb-3">
      <FormControl
        type="search"
        placeholder="Filter by name"
        aria-label="Filter countries"
        onKeyUp={(event: any) => onFilter(event.currentTarget.value)}
      />
      <InputGroup.Append>
        <InputGroup.Text>
          <FontAwesomeIcon icon={faSearch} />
        </InputGroup.Text>
      </InputGroup.Append>
    </InputGroup>
  );
};
