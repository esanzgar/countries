import React from 'react';
import { shallow } from 'enzyme';

import { FilterCountry } from './filter-country';

describe('<FilterCountry />', () => {
  it('should display', () => {
    const wrapper = shallow(<FilterCountry onFilter={keyword => null} />);
    expect(wrapper).toMatchSnapshot();
  });
});
