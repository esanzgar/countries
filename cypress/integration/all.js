// enables intelligent code completion for Cypress commands
// https://on.cypress.io/intelligent-code-completion
/// <reference types="Cypress" />

context('Home page', () => {
  beforeEach(() => {
    cy.visit('');
    cy.get('tbody > tr').as('countries');
  });

  it('should look ok', function() {
    cy.contains('Countries of the world').should('be.visible');
    cy.get('@countries').should('have.length', 250);
    cy.url().should('all');
  });

  it('type in the filter', function() {
    cy.get('input').type('chi{enter}');
    cy.get('@countries').should('have.length', 2);
    cy.get('input').clear();
    cy.get('@countries').should('have.length', 250);
  });

  it('should sort', function() {
    cy.get('@countries')
      .first()
      .contains('Afghanistan');
    cy.get('tr > :nth-child(2) > .cursor').click();
    cy.get('@countries').should('have.length', 250);
    cy.get('tbody > tr')
      .first()
      .contains('Zimbabwe');

    cy.get('tr > :nth-child(3) > .cursor').click();
    cy.get('@countries').should('have.length', 250);
    cy.get('tbody > tr')
      .first()
      .contains('China');
  });

  it('should navigate to detailed page', function() {
    cy.get('@countries')
      .first()
      .click();
    cy.url().should('include', 'country/AFG');
    cy.get('h2').contains('Afghanistan');
  });
});
