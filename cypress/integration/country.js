// enables intelligent code completion for Cypress commands
// https://on.cypress.io/intelligent-code-completion
/// <reference types="Cypress" />

context('Detailed page', () => {
  beforeEach(() => {
    cy.visit('country/AFG');
  });

  it('should look ok', function() {
    cy.url().should('include', 'country/AFG');
    cy.contains('Countries of the world').should('be.visible');
    cy.get('h2').contains('Afghanistan');
  });

  it('should navigate to another detailed page', function() {
    cy.get('ul > :nth-child(1) > a')
      .first()
      .click();
    cy.url().should('include', 'country/IRN');
    cy.get('h2').contains('Iran');
  });

  it('should navigate to home page', function() {
    cy.get('.card-link').click();
    cy.url().should('include', 'all');
  });
});
