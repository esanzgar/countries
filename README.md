# Countries

A Typescript/React application to list the countries of the world and display
information about each country.

The app can be accessed at
[here](https://esanzgar.gitlab.io/countries).

## Development server

To develop locally run the development server following one of these recipes:

```
yarn install --frozen-lockfile
yarn start
```

To run unit testing:

```
yarn test
```

To run integration testing:

```
# First, stop development server, then
yarn start-server-and-test
```

## Log

- Monday, 14 Oct. 2019: 7-8pm initial setup project
- Tuesday, 15 Oct. 2019: 6:30-8pm setup initial functionality
- Wednesday, 16 Oct. 2019:
  - 10:30-11am initial setup of map visualisation
  - 11-12, 2-4pm sorting an filter of first table
  - 4:30-8:00pm emojis for flags and optimization (dictionary structure,
    preloading of data, fixes, etc). I am implemented all the needed
    functionality.
- Thursday, 17 Oct. 2019:
  - 8:15-8:30am, 3-6pm styling
  - 6-7pm icon font and few more styling and fixes.
- Friday, 18 Oct. 2019:
  - 8-9am, 3-3:45pm incorporated more changes based on user feedback (family and
    colleagues)
  - 3:45-4:45 lazy loading and app shell
  - 4:45-6pm unit testing of services
  - 9-9:30pm fix issues with shell and routing
  - 10-11pm unit testing of components
- Saturday, 19 Oct. 2019:
  - 7-8am unit testing of components
  - 10-11am some refactoring and UI improvements
  - 2-3pm fix an issue updating values
  - 3-4pm e2e cypress testing

## Retrospective

The web app has been written in TypeScript and the latest version of React
(16.10). It features 2 lazy loading routes (`/all` and `/country/[3 letter code]`).
The theming and look and feel is from react-bootstrap which is 100% responsive
design (for mobile and desktop). All the components are functional components
and they use react hooks. The application is a progressive web app, although it
lacks a prompt for home screen installation. Gitlab CI/CD was used for continuous
integration/deployment. Note: Gitlab pages don't serve the css and js assets
compressed.

### Performance:

- Data loading and data structures: The app is preloaded with a set of countries
  and it fetches the rest API on the background. All user interactions
  (filtering or sorting) are applied to the new data obtained so the user is
  unaware. You can detect this by looking at the change on population for
  Afghanistan (preloaded: 27,657,144, after fetching: 27,657,145). The list of
  countries are converted internally in a dictionary for constant runtime
  access.

  - Pros: Data is displayed on first rendering.

  - Cons: Preloading data increases the size of the bundle. However, this data
    is compressible. Additionally, the minimum amount, for the most frequent
    screen size (based on some usage analytics), can be used.

- Flags: I chose to display flags as emojis instead of image.

  - Pros: It is the fastest possible option. To further speed up the
    calculation for the unicodes I cache the result using memoisation.

  - Cons: emojis don't look the same in all web browsers. If
    consistent looking is required, then I suggest using a sprite image,
    which warranties only the loading of one image. Alternatively, a
    pagination with a small page size could render fast.

- World map: I chose the quickest to implement and lighter option (<150kB).

  - Pros: the svg map renders well at all resolution.

  - Cons: it lacks options to zoom in/out or displaying a circle around small
    countries. On the other hand, these should not be difficult to
    implement.

  - Good feature complete alternatives:
    - https://www.react-simple-maps.io/examples/
    - https://github.com/google-map-react/google-map-react

- Lighthouse audits and total size: The application code is slim and has a good
  performance (even in slow 4G) and accesibility. Note: Gitlab pages don't
  compress content, so the performance index doesn't reflect the best possible
  scenario.
  ![performance slow 4G](performance-slow-4G.png)

```
107.51 KB (-26 B) build/static/js/main.152e1356.chunk.js
102.43 KB (-5 B) build/static/js/2.49e5fd15.chunk.js
2.45 KB (+28 B) build/static/js/4.5bbf05b2.chunk.js
1.66 KB (+25 B) build/static/js/3.8e4d90b8.chunk.js
1.49 KB (-2 B) build/static/js/runtime-main.3515c070.js
610 B build/static/css/main.967b2040.chunk.css
164 B build/static/css/3.7ee2b4ff.chunk.css
121 B build/static/css/4.ddaeb4cb.chunk.css
```

### Test

The application contains both unit and integration test, the later using
cypress.io. At the moment, it is fairly minimal and doesn't reach the threshold
of 80%, set up initially. Sorry, ☹️.

### Bells and whistles

- Toaster notification: if errors fetching the data or offline conditions arise
  the application pops up a notification. Error fetching conditions are
  detected via Axios interceptors.

- Loading indicator: a top infinite progress bar indicates ongoing fetching
  events. Again this is implemented via Axios interceptors.

- Fontawesome icons: I added some screen reading accesibility for the visually
  impaired.

### Other ideas and comments

This app is a good candidate for a Gastby. Each country page could be
pre-rendered for fast loading.

The app doesn't have internationalization (i18n) but it supports localization
(I10n) using locale functions and international collator for sorting, filtering
and displaying of group numbers.

Tables are good for shorting and filtering. However, user feedback I had showed
that is not a intutitive for navigation. An alternative home page could a list
of summary country cards with an alphabetical pagination (however, filtering by
population would not make sense). `react-bootstrap-table` could have been used
for multiple sorts.

To avoid content shifting a placeholder of the same size of the map should be
used.
